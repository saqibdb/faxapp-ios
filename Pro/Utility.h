//
//  Utility.h
//  PiggieBack
//
//  Created by ibuildx-Mac on 5/17/17.
//  Copyright © 2017 Abc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
+(BOOL) NSStringIsValidEmail:(NSString *)checkString ;
@end
