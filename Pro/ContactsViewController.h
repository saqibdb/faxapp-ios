//
//  ContactsViewController.h
//  Pro
//
//  Created by ibuildx-Mac on 7/19/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol selectContactNumber <NSObject>
@optional;
-(void)receicveContactNo:(NSString *)phoneNo;
@end


@interface ContactsViewController : UIViewController

@property (nonatomic, strong) id<selectContactNumber> delegate; // Delegate Id



@property (strong ,nonatomic) NSMutableArray *arrayContacts;

@property (strong, nonatomic) IBOutlet UITableView *tblViewContacts;
@end
