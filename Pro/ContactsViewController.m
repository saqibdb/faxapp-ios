//
//  ContactsViewController.m
//  Pro
//
//  Created by ibuildx-Mac on 7/19/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import "ContactsViewController.h"
#import "ContactsTableViewCell.h"
#import <ContactsKit/ContactsKit.h>
#import <Contacts/Contacts.h>
#import "SVProgressHUD.h"
#import "ContactsWrapper.h"

@interface ContactsViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:NO];

    NSLog(@"%@",_arrayContacts);
    
    self.tblViewContacts.delegate = self;
    self.tblViewContacts.dataSource = self;
    [self getContactsNew];
    //[self RecieveContacts];
}

-(void)getContactsNew {
    [[ContactsWrapper sharedInstance] getContactsWithContainerId:nil completionBlock:^(NSArray<CNContact *> * _Nullable contacts, NSError * _Nullable error) {
        if (contacts)
        {
            //self.contacts = [NSMutableArray arrayWithArray:contacts];
            //[self.tableView reloadData];
            self.arrayContacts = [[NSMutableArray alloc] initWithArray:contacts];
            [_tblViewContacts reloadData];
            
            
            
            
            NSLog(@"TOTAL CONTACTS = %i", contacts.count);
        }
        else
        {
            NSLog(@"%@", error.localizedDescription);
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayContacts.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CNContact *contact=  [_arrayContacts objectAtIndex:indexPath.row];
    NSString *phoneNo = [[[contact.phoneNumbers firstObject] value] stringValue];
    
    [self.delegate receicveContactNo:phoneNo];
    
    [self.navigationController popViewControllerAnimated:YES];


}
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactsTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
 
     CNContact *contact=  [_arrayContacts objectAtIndex:indexPath.row];
     
     cell.lblName.text = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
     cell.lblPhoneNumberf.text=[[[contact.phoneNumbers firstObject] value] stringValue];
 
 return cell;
 }

-(void)RecieveContacts
{
   
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey,  CNContactFamilyNameKey, CNContactGivenNameKey,CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, nil];
         [SVProgressHUD show];
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * contact, BOOL *stop) {
            
            
            NSString *phoneNumber = @"";
            if( contact.phoneNumbers){
                phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
                
                [_arrayContacts addObject:contact];
                NSLog(@"%@",_arrayContacts);
                [SVProgressHUD dismiss];
            }
            
            
            
        }];
        
        NSLog(@"%@",_arrayContacts);
        //[self performSegueWithIdentifier:@"ViewControllerToContactsSegue" sender:self];
    

    
    
}


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
