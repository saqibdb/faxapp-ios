//
//  ViewController.h
//  Pro
//
//  Created by ibuildx-Mac on 7/3/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsViewController.h"



@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate , UITableViewDelegate , UITableViewDataSource,UITextFieldDelegate >












- (IBAction)phoneBookAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *fileTableView;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;

- (IBAction)sendFaxAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberText;
@property (strong, nonatomic) IBOutlet UIImageView *flagImg;
- (IBAction)countryCodeAction:(id)sender;

- (IBAction)addFileAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *countryCodeLable;




@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end

