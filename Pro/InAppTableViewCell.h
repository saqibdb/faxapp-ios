//
//  InAppTableViewCell.h
//  Fax Now
//
//  Created by ibuildx on 7/24/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InAppTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *inAppDescription;
@property (weak, nonatomic) IBOutlet UILabel *inAppTitle;

@end
