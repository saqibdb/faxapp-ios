//
//  fileImgViewController.h
//  Pro
//
//  Created by ibuildx-Mac on 7/7/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface fileImgViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *fileImg;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;

@end
