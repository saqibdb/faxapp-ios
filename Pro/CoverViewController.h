//
//  CoverViewController.h
//  Fax Now
//
//  Created by ibuildx-Mac on 7/26/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CoverViewController : UIViewController

{
        NSDictionary *data;
}
- (IBAction)coverAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbl_date;
@property (strong, nonatomic) IBOutlet UILabel *lblFrom;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Contact;
@property (strong, nonatomic) IBOutlet UILabel *lbl_message;
@property(nonatomic,retain) NSDictionary *CoverInfo;

@end
