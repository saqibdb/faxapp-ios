//
//  ContactsTableViewCell.h
//  Pro
//
//  Created by ibuildx-Mac on 7/19/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNumberf;


@end
