
//
//  CoverViewController.m
//  Fax Now
//
//  Created by ibuildx-Mac on 7/26/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import "CoverViewController.h"

@interface CoverViewController ()
@end
@implementation CoverViewController
@synthesize CoverInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
        // Do any additional setup after loading the view.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm a"];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    
    NSString *stringFromDate = [formatter stringFromDate: [CoverInfo valueForKey:@"createdDate"]];
    self.lbl_date.text = stringFromDate;
    self.lblFrom.text = [CoverInfo valueForKey:@"from"];
    self.lbl_Contact.text = [CoverInfo valueForKey:@"contact"];
    self.lbl_message.text = [CoverInfo valueForKey:@"message"];
   
}
-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)coverAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
