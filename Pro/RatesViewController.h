//
//  RatesViewController.h
//  Pro
//
//  Created by ibuildx-Mac on 7/10/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "IAPCreditsSystem.h"

@interface RatesViewController : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver, UITableViewDelegate, UITableViewDataSource>
{
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    UIActivityIndicatorView *activityIndicatorView;
    IBOutlet UILabel *productTitleLabel;
    IBOutlet UILabel *productDescriptionLabel;
    IBOutlet UILabel *productPriceLabel;
    IBOutlet UIButton *purchaseButton;
}

- (void)fetchAvailableProducts;
- (BOOL)canMakePurchases;
- (void)purchaseMyProduct:(SKProduct*)product;
- (IBAction)purchase:(id)sender;




@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet NSString *faxNumber;

@property (strong, nonatomic) IBOutlet UILabel* balanceLabel;


@property (weak, nonatomic) IBOutlet UITableView *iAPTableView;






@end
