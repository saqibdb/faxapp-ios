//
//  IAPCreditsSystem.h
//  Fax Now
//
//  Created by Umer naseer on 18.08.17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kProductId50Credits @"faxnow1" //$3.99
#define kProductId75Credits @"faxnow2" //$4.99
#define kProductId100Credits @"faxnow3" //$5.99
#define kProductId250Credits @"faxnow4" //$9.99
#define kProductId600Credits @"faxnow5" //$19.99
#define kProductId2000Credits @"faxnow6" //$49.99

@interface IAPCreditsSystem : NSObject

@property (assign, nonatomic) NSInteger balance;

+ (instancetype)sharedInstance;
- (void)addCreditsOnBalance:(NSInteger)credits;
- (void)withdrawCreditsFromBalance:(NSInteger)credits;
- (NSInteger)creditsCountByProductID:(NSString*)productID;
- (NSInteger)currentCredits;

@end
