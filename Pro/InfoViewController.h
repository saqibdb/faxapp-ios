//
//  InfoViewController.h
//  Pro
//
//  Created by ibuildx-Mac on 7/13/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController <UITableViewDelegate , UITableViewDataSource >
@property (strong, nonatomic) IBOutlet UITableView *infoTableView;

@end
