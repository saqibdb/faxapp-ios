//
//  fileTableViewCell.h
//  Pro
//
//  Created by ibuildx-Mac on 7/6/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface fileTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *closeCellBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imgCover;
@property (strong, nonatomic) IBOutlet UILabel *lblCover;



@end
