//
//  IAPCreditsSystem.m
//  Fax Now
//
//  Created by Umer naseer  on 18.08.17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import "IAPCreditsSystem.h"

#define kCreditsBalance @"kCreditsBalance"

@implementation IAPCreditsSystem

+ (instancetype)sharedInstance {
    static IAPCreditsSystem *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[IAPCreditsSystem alloc] init];
        sharedInstance.balance = [[NSUserDefaults standardUserDefaults]integerForKey:kCreditsBalance];
    });
    return sharedInstance;
}

- (void)addCreditsOnBalance:(NSInteger)credits {
    self.balance += credits;
    [[NSUserDefaults standardUserDefaults]setInteger:self.balance forKey:kCreditsBalance];
}

- (void)withdrawCreditsFromBalance:(NSInteger)credits {
    self.balance -= credits;
    [[NSUserDefaults standardUserDefaults]setInteger:self.balance forKey:kCreditsBalance];
}

- (NSInteger)creditsCountByProductID:(NSString*)productID {
    if([productID isEqualToString:kProductId50Credits]){
        return 50;
    }else if([productID isEqualToString:kProductId75Credits]){
        return 75;
    }else if([productID isEqualToString:kProductId100Credits]){
        return 100;
    }else if([productID isEqualToString:kProductId250Credits]){
        return 250;
    }else if([productID isEqualToString:kProductId600Credits]){
        return 600;
    }else if([productID isEqualToString:kProductId2000Credits]){
        return 2000;
    }
    return 0;
}

- (NSInteger)currentCredits {
    return [[NSUserDefaults standardUserDefaults]integerForKey:kCreditsBalance];
}

@end
