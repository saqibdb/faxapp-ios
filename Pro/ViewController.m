//
//  ViewController.m
//  Pro
//
//  Created by ibuildx-Mac on 7/3/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import "ViewController.h"
#import "PCCPViewController.h"
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "fileTableViewCell.h"
#import "fileImgViewController.h"
#import "UIViewController+CWPopup.h"
#import "RatesViewController.h"
#import "SVProgressHUD.h"
#import <ContactsKit/ContactsKit.h>
#import "ContactsViewController.h"
#import "ContactsTableViewCell.h"
#import <Contacts/Contacts.h>
#import "CoverPageTableViewController.h"
#import "CoverViewController.h"
#import "TabBarViewController.h"

#define api_key @"ddf1a780-54cb-11e7-948a-dfe6a04ac492"

@interface ViewController () 
{
    NSMutableArray *fileImges;
    NSMutableArray *arrayContact;
    fileImgViewController *fileImage;
    UIImage *selectedImage;
    int selectedIndex;
    BOOL isPopup;
    
    NSString *selectedFileName;
    UITapGestureRecognizer *singleFingerTap;
    
    CKAddressBook *addressBook;
    
    BOOL isCoverPage;
    NSDictionary *coverInfoDict;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isCoverPage = NO;
    
    arrayContact = [[NSMutableArray alloc] init];
    self.fileTableView.delegate = self;
    self.fileTableView.dataSource = self;
    self.phoneNumberText.delegate = self;
    fileImges = [[NSMutableArray alloc] init];
    _mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, _mainScrollView.frame.size.height + 300);
    
    isPopup = NO;
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
///////////text field handler/////////////

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
    [self createInstanceOfContacts];
}

-(void)createInstanceOfContacts {
    addressBook = [[CKAddressBook alloc] init];
    
    [addressBook requestAccessWithCompletion:^(NSError *error) {
        
        if (! error)
        {
            // Everything fine you can get contacts
            
        }
        else
        {
            // The app doesn't have a permission for getting contacts
            // You have to go to the settings and turn on contacts
        }
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // All digits entered
    if (range.location == 12) {
        return NO;
    }
    
    // Reject appending non-digit characters
    if (range.length == 0 &&
        ![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
        return NO;
    }
    
    // Auto-add hyphen before appending 4rd or 7th digit
    if (range.length == 0 &&
        (range.location == 3 || range.location == 7)) {
        textField.text = [NSString stringWithFormat:@"%@-%@", textField.text, string];
        return NO;
    }
    
    // Delete hyphen when deleting its trailing digit
    if (range.length == 1 &&
        (range.location == 4 || range.location == 8))  {
        range.location--;
        range.length = 2;
        textField.text = [textField.text stringByReplacingCharactersInRange:range withString:@""];
        return NO;
    }
    
    return YES;
    
    /*
    
    NSUInteger newLength = [_phoneNumberText.text length] + [string length] - range.length;
    if (newLength > 10)
    {
        return NO;
        
    }
    
    if(newLength == 10 )
    {
        
        int groupingSize = 3;
        if([string length] == 0)
        {
            groupingSize = 4;
        }
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
        NSString *separator = @"-";
        [formatter setGroupingSeparator:separator];
        [formatter setGroupingSize:groupingSize];
        [formatter setUsesGroupingSeparator:YES];
        [formatter setSecondaryGroupingSize:3];
        if (![string  isEqual: @""] && ( _phoneNumberText.text != nil && _phoneNumberText.text.length > 0))
        {
            NSString *num = _phoneNumberText.text;
            num = [num stringByReplacingOccurrencesOfString:separator withString:@""];
            NSString *str = [formatter stringFromNumber:[NSNumber numberWithDouble:[num doubleValue]]];
            _phoneNumberText.text = str;
        }
        
        
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Wrong Fax  Number Entered" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:okAction];
        
        
    }
    return YES;
     */
}





- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    //Do stuff here...
    
    if (isPopup) {
        isPopup = NO;
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
            [self.view removeGestureRecognizer:singleFingerTap];
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendFaxAction:(id)sender {
    if ([_phoneNumberText.text isEqualToString:@""] || _phoneNumberText.text.length != 12) {
    
        [self showErrorWithMessage:@"FAX" message:@"Enter Correct Fax Number"];
        return;
    }
    
    if (!fileImges.count) {
        [self showErrorWithMessage:@"FAX" message:@"Select Image/File"];
        return;
    }
    
    [self checkBalanceAPI];
}

-(void)checkBalanceAPI {
    
    IAPCreditsSystem* iapManager = [IAPCreditsSystem sharedInstance];
    //if ([iapManager currentCredits]) {
        [self fileuploadAPI2];
    //}
    
    /*NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"a1b6e938-bbdf-8fec-f512-fd2744058aff" };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://fax.to/api/v1/balance?api_key=ddf1a780-54cb-11e7-948a-dfe6a04ac492"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    [SVProgressHUD showWithStatus:@"Calculating Cost."];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (error) {
                
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
            } else {
                [SVProgressHUD dismiss];
                NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                NSLog(@"%@", responseDict);
                
                NSString *balanceStr = [responseDict valueForKey:@"balance"];
                int balance = [balanceStr intValue] + 10;
                
                if (balance) {
                    [self fileuploadAPI2];
                }
                else {
                    [self showErrorWithMessage:@"FAX" message:@"NO BALANCE"];
                }
                
            }
        });
        
    }];
    [dataTask resume];*/
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)fileuploadAPI2 {
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://fax.to/api/v1/files?api_key=ddf1a780-54cb-11e7-948a-dfe6a04ac492"]];
    
    NSData *imageData = UIImageJPEGRepresentation([self imageWithImage:selectedImage scaledToSize:CGSizeMake(200, 200)], 1.0);
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"imageCaption"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add image data
    if (imageData) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=file; fileName=imageNam0000.jpg\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [SVProgressHUD showWithStatus:@"Calculating Cost."];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(data.length > 0)
            {
                [SVProgressHUD dismiss];
                
                //NSLog(@"%@",response);
                
                NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                NSLog(@"%@", responseDict) ;
                
                
                if ([responseDict objectForKey:@"document_id"]) {
                    NSString *documentId = [responseDict objectForKey:@"document_id"];
                    NSString *apiKey = @"ddf1a780-54cb-11e7-948a-dfe6a04ac492";
                    
                    NSString *faxNumber =[_countryCodeLable.text stringByAppendingString:_phoneNumberText.text];
                    [self FaxCostAPI:apiKey documentId:documentId faxNumber:faxNumber];
                }
                else{
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Document Id not Not " andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                    
                    
                }
            }
            
            else
            {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@" Error" andText:@"File Not Uploaded" andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
        });
        
        
        
            
            
    }];
}

- (void)searchGroupsAtCoordinate
{
    NSString *urlAsString = [NSString stringWithFormat:@"https://fax.to/api/v1/fax?api_key=ddf1a780-54cb-11e7-948a-dfe6a04ac492&document_id=112601&fax_number=888-371-3290&charset=UTF-8"];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    NSLog(@"%@", urlAsString);
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
            NSLog(@"%@", error) ;

        } else {
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            NSLog(@"%@", responseDict) ;

        }
    }];
}

-(void) FaxCostAPI:(NSString *)apiKey documentId:(NSString *)documentId faxNumber:(NSString *)faxNumber
{
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"6803fcbc-f22d-724f-8f9e-eb0a3508e534" };
    
    
    NSString *reqURL = [NSString stringWithFormat:@"https://fax.to/api/v1/fax/%@/costs?api_key=ddf1a780-54cb-11e7-948a-dfe6a04ac492&fax_number=%@",documentId,faxNumber];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:reqURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    
    
    
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];

    [SVProgressHUD showWithStatus:@"Calculating Cost."];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText: error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            
        } else {
            [SVProgressHUD dismiss];
            

            
            
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            NSLog(@"%@", responseDict) ;
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([[responseDict valueForKey:@"status"] isEqualToString:@"success"]) {
                    float retaitCost = [[responseDict valueForKey:@"cost"] floatValue];
                    retaitCost = retaitCost * 100;
                    
                    int creditsCost = (int)retaitCost; //TODO credits cost. You need to convert retaitCost to creditsCost.
                    
                    IAPCreditsSystem* iapManager = [IAPCreditsSystem sharedInstance];
                    
                    if([iapManager currentCredits] >= creditsCost){
                        NSString *msg = [NSString stringWithFormat:@"FAX WILL COST: %d Credits. Do you want to countinue?",(int)retaitCost];
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"FAX Cost" message:msg preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){[alert dismissViewControllerAnimated:YES completion:nil];
                            [self sendFaxAPI:apiKey documentId:documentId faxNumber:faxNumber creditsCost:creditsCost];
                        }];
                        UIAlertAction* cancelButton = [UIAlertAction
                                                       actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                                       {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           
                                                           
                                                           [self deleteFileWithId:documentId];
                                                       }];
                        [alert addAction:cancelButton];
                        [alert addAction:okButton];
                        
                        [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        [self deleteFileWithId:documentId];
                        
                        NSString *msg = [NSString stringWithFormat:@"Not enough credits!"];
                        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Error!" message:msg preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* cancelButton = [UIAlertAction
                                                       actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                                       {
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                           TabBarViewController *tbvc = (TabBarViewController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
                                                           [tbvc setSelectedIndex:1];
                                                       }];
                        [alert addAction:cancelButton];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
                else{
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Fail to Calculate Fax Cost" andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                    
                    
                }

            });
            
            
        }
    }];
    [dataTask resume];
}

-(void)deleteFileWithId :(NSString *)docId {
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"1358de93-bfbd-a2ac-944d-eb10454673ef" };
    
    NSString *strUrl = [NSString stringWithFormat:@"https://fax.to/api/v1/files/%@?api_key=ddf1a780-54cb-11e7-948a-dfe6a04ac492",docId ];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"DELETE"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
        }
    }];
    [dataTask resume];
}


-(void)sendFaxAPI:(NSString *)apiKey documentId:(NSString *)documentId faxNumber:(NSString *)faxNumber creditsCost:(NSInteger)cost {
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"8fa500f1-814f-cbc7-6d0b-8001852b9346" };
    
    
    NSString *reqURL = [NSString stringWithFormat:@"https://fax.to/api/v1/fax?api_key=ddf1a780-54cb-11e7-948a-dfe6a04ac492&fax_number=%@&document_id=%@",faxNumber,documentId];

    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:reqURL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    [SVProgressHUD showWithStatus:@"Sending Fax."];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            
            [SVProgressHUD dismiss];
            ////////////////
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.localizedDescription andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
            
            //////////////
            
        } else {
            [SVProgressHUD dismiss];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
            
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            NSLog(@"%@", responseDict) ;
            
            
            if ([[responseDict valueForKey:@"status"] isEqualToString:@"executed"]) {
                [[IAPCreditsSystem sharedInstance]withdrawCreditsFromBalance:cost];
                [self showErrorWithMessage:@"FAX" message:@"Fax sent successfully"];
            }
            else{
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Fail to send Fax " andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
                
            }
        }
    }];
    [dataTask resume];
}

- (IBAction)countryCodeAction:(id)sender {
    NSDictionary * countryDic = [PCCPViewController infoFromSimCardAndiOSSettings];
    UIImage * flag = [PCCPViewController imageForCountryCode:countryDic[@"country_code"]];
    NSLog(@"%@", countryDic);
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        NSLog(@"%@", countryDic);
        UIImage *flag = [PCCPViewController imageForCountryCode:countryDic[@"country_code"]];
        
        NSDictionary *countryDict = [countryDic mutableCopy];
        
        
        NSString *stringCode = [NSString stringWithFormat:@"%@",[countryDict valueForKey:@"phone_code"]];
        
        _countryCodeLable.text = stringCode;
        _flagImg.image=flag;
    }];
    [vc setIsUsingChinese:NO];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    //[self presentViewController:vc animated:YES completion:NULL];
}

#pragma mark - ActionSheet delegates


- (void)showCameraImagePicker {
#if TARGET_IPHONE_SIMULATOR
   #elif TARGET_OS_IPHONE
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
#endif
}

- (void)showGalleryImagePicker {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


    


#pragma mark - PickerDelegates
    
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    selectedImage = info[UIImagePickerControllerOriginalImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    selectedFileName = info[UIImagePickerControllerReferenceURL];
    if (fileImges.count == 0) {
        _tableViewHeightConstraint.constant  = _tableViewHeightConstraint.constant;
    }
    else
    {
        _tableViewHeightConstraint.constant  = _tableViewHeightConstraint.constant + 63;
        _mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, _mainScrollView.frame.size.height+ 300);
        
        
        //_mainScrollView.contentSize.height += 63 ;
//        _mainScrollView.contentSize.frame.height = _mainScrollView.contentSize.frame.height + 63 ;
    }

    [fileImges addObject:chosenImage];
       [self.fileTableView beginUpdates];
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[fileImges count]-1 inSection:0]];
    [self.fileTableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
    [self.fileTableView endUpdates];
    [picker dismissViewControllerAnimated:YES completion:nil];

    
}
     ////////////////////TableView Methods
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    if (!isCoverPage) {
//        return 1;
//    }
//    else {
//        return 1;
//    }
//    
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [fileImges count];

//    if (!isCoverPage) {
//        return [fileImges count];
//    }
//    else {
//        return 1;
//    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"fileUpload";
    fileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    
    if ([[fileImges objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
        
        [cell.closeCellBtn setHidden:YES];
        [cell.imgCover setHidden:NO];
        [cell.lblCover setHidden:NO];
        
        cell.imgCover.image = [UIImage imageNamed:@"coverPage"];
        cell.lblCover.text = @"Cover Page";
        return cell;
    }
    else {
        [cell.lblCover setHidden:YES];
        [cell.closeCellBtn setHidden:NO];
        [cell.imgCover setHidden:NO];
        
        cell.imgCover.image = [fileImges objectAtIndex:indexPath.row];
        //cell.textLabel.text = [fileImges objectAtIndex:indexPath.row];
        [cell.closeCellBtn addTarget:self action:@selector(deleteSelectedFile:) forControlEvents:UIControlEventTouchUpInside];
        cell.closeCellBtn.tag = indexPath.row;
        return cell;

    }
    
    
    
    /*
    if (!isCoverPage) {
        static NSString *MyIdentifier = @"fileUpload";
        fileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        [cell.lblCover setHidden:YES];
        [cell.closeCellBtn setHidden:NO];
        [cell.imgCover setHidden:NO];

        cell.imgCover.image = [fileImges objectAtIndex:indexPath.row];
        //cell.textLabel.text = [fileImges objectAtIndex:indexPath.row];
        [cell.closeCellBtn addTarget:self action:@selector(deleteSelectedFile:) forControlEvents:UIControlEventTouchUpInside];
        cell.closeCellBtn.tag = indexPath.row;
        return cell;
    }
    else {
        static NSString *MyIdentifier = @"fileUpload";
        fileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        [cell.closeCellBtn setHidden:YES];
        [cell.imgCover setHidden:NO];
        [cell.lblCover setHidden:NO];
        
        cell.imgCover.image = [UIImage imageNamed:@"coverPage"];
        cell.lblCover.text = @"Cover Page";
        return cell;
    }
     */
}

- (void)deleteSelectedFile:(UIButton *)sender
{
    
    [fileImges removeObjectAtIndex:sender.tag];
    [_fileTableView reloadData];
    if (fileImges.count == 0) {
        _tableViewHeightConstraint.constant  = _tableViewHeightConstraint.constant;
    }
    else
    {
    _tableViewHeightConstraint.constant  = _tableViewHeightConstraint.constant - 63;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[fileImges objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]) {
        [self performSegueWithIdentifier:@"SendFaxToCoverPageTableSegue" sender:self];
    }
    else {
        selectedImage = [fileImges objectAtIndex:indexPath.row];
        
        if (!isPopup) {
            [self addPopUp];
        }
    }
}

- (void) addPopUp {
    fileImage = [[fileImgViewController alloc] initWithNibName:@"fileImgViewController" bundle:nil];
    fileImage.view.frame
    = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width-50,self.view.frame.size.height * 0.7);
    
    
    fileImage.fileImg.image = selectedImage;
    
     [fileImage.closeBtn addTarget:self action:@selector(closeBtn) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self presentPopupViewController:fileImage animated:YES completion:^(void) {

    }];
    
    if (!isPopup) {
        isPopup = YES;

    singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    singleFingerTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleFingerTap];
        }
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
   
    
    if ([segue.identifier isEqualToString:@"MainToRatesSegue"]) {
        RatesViewController *ratesvc = [segue destinationViewController];
        ViewController *vc = [[ViewController alloc] init];

        NSString *faxNumber =[vc.countryCodeLable.text stringByAppendingString: vc.phoneNumberText.text];
        ratesvc.img = fileImage.fileImg;
        ratesvc.faxNumber = faxNumber;
    }
    else if ([segue.identifier isEqualToString:@"ViewControllerToContactsSegue"])
    {
        
        ContactsViewController *contact = [segue destinationViewController];
        contact.arrayContacts = [arrayContact mutableCopy];
        contact.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"FaxToCoverPageSegue"]) {
        CoverPageTableViewController *cdv = segue.destinationViewController;
        cdv.delegate = self;
    }
    
    else if ([segue.identifier isEqualToString:@"SendFaxToCoverPageTableSegue"]) {
        CoverViewController *CV = segue.destinationViewController;
        CV.CoverInfo = coverInfoDict;
        
        
    }

    
}

- (IBAction)addFileAction:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add image or document (PDF,DOC)" message:@" " preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction *CoverPage = [UIAlertAction actionWithTitle:@"Cover Page" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self performSegueWithIdentifier:@"FaxToCoverPageSegue" sender:self];
     
    }];

    UIAlertAction *AlbumAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        isCoverPage = NO;
        [self showGalleryImagePicker];

        
       
        
    }];
    UIAlertAction *CameraAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        isCoverPage = NO;
        [self showCameraImagePicker];

    }];
    [alert addAction:CoverPage];
    [alert addAction:okAction];
    [alert addAction:AlbumAction];
    [alert addAction:CameraAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Show Message Alert
-(void)showErrorWithMessage:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)phoneBookAction:(id)sender {
    
    [self performSegueWithIdentifier:@"ViewControllerToContactsSegue" sender:self];
    }

-(void)receicveContactNo:(NSString *)phoneNo {
    NSLog(@"%@",phoneNo);
    
    self.phoneNumberText.text = phoneNo;
}


-(void)receicveCoverInfo:(NSDictionary *)infoDict {
    isCoverPage = YES;
    NSLog(@"DICT %@",infoDict);
    
    coverInfoDict = [[NSDictionary alloc] init];
    coverInfoDict = [infoDict mutableCopy];
    
    if (fileImges.count == 0 )
    {
        
        [fileImges insertObject:coverInfoDict atIndex:0];
        _tableViewHeightConstraint.constant  = _tableViewHeightConstraint.constant;
    }
    else
    {
        for (id value in fileImges){
            if ([value isKindOfClass:[NSDictionary class]]){
                [fileImges replaceObjectAtIndex:0 withObject:coverInfoDict];
                
                break;
            }
            else {
                [fileImges insertObject:coverInfoDict atIndex:0];
                _tableViewHeightConstraint.constant  = _tableViewHeightConstraint.constant + 63;
                _mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, _mainScrollView.frame.size.height+ 300);
            }
        }
    }
//    if (fileImges.count == 0) {
//        
//    }
//    else
//    {
//        
//        
//        
//        //_mainScrollView.contentSize.height += 63 ;
//        //        _mainScrollView.contentSize.frame.height = _mainScrollView.contentSize.frame.height + 63 ;
//    }

    
    
    [self.fileTableView reloadData];
}



@end
