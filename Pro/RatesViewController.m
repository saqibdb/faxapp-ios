//
//  RatesViewController.m
//  Pro
//
//  Created by ibuildx-Mac on 7/10/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import "RatesViewController.h"
#import "SVProgressHUD.h"
#import "InAppTableViewCell.h"
#import "SVProgressHUD.h"
#import "IAPCreditsSystem.h"

#define kTutorialPointProductID @"com.saqibdb.faxnow"

@interface RatesViewController ()

@end

@implementation RatesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.iAPTableView.delegate = self;
    self.iAPTableView.dataSource = self;
    
    [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [self.iAPTableView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    
    IAPCreditsSystem* iapManager = [IAPCreditsSystem sharedInstance];
    self.balanceLabel.text = [NSString stringWithFormat:@"Your credits balance  %ld credits", (long)[iapManager currentCredits], nil];

    [self fetchAvailableProducts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [validProducts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"InAppTableViewCell";
    InAppTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[InAppTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:CellIdentifier];
    }
    

    SKProduct *validProduct = [validProducts objectAtIndex:indexPath.row];
    cell.inAppDescription.text = [NSString stringWithFormat:@"%@ for %@", validProduct.localizedTitle, validProduct.price];
    cell.inAppTitle.text = [NSString stringWithFormat:@"%@", validProduct.localizedDescription];
    
    return cell ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self purchaseMyProduct:[validProducts objectAtIndex:indexPath.row]];
}



-(void)fetchAvailableProducts{
    /* delete kTutorialPointProductID bellow (needed for testing) */
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:kTutorialPointProductID, kProductId50Credits, kProductId75Credits, kProductId100Credits, kProductId250Credits, kProductId600Credits, kProductId2000Credits, nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
    [SVProgressHUD showWithStatus:@"Fetching Prices."];
}

- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        [SVProgressHUD showWithStatus:@"Purchasing Order."];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}
-(IBAction)purchase:(id)sender{
    [self purchaseMyProduct:[validProducts objectAtIndex:0]];
    purchaseButton.enabled = NO;
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased: {
//                if ([transaction.payment.productIdentifier
//                     isEqualToString:kTutorialPointProductID]) {
//                    NSLog(@"Purchased ");
//                    [SVProgressHUD dismiss];
//                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
//                                              @"Purchase is completed succesfully" message:nil delegate:
//                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                    [alertView show];
//                }
                IAPCreditsSystem* iapManager = [IAPCreditsSystem sharedInstance];
                [iapManager addCreditsOnBalance:[iapManager creditsCountByProductID:transaction.payment.productIdentifier]];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                self.balanceLabel.text = [NSString stringWithFormat:@"Your credits balance  %ld credits", (long)[iapManager currentCredits], nil];
                [SVProgressHUD dismiss];

                break;
            }
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored ");
                [SVProgressHUD dismiss];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                [SVProgressHUD dismiss];
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    [SVProgressHUD dismiss];
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    if (count>0) {
        validProducts = response.products;
        [self.iAPTableView reloadData];
        validProduct = [response.products objectAtIndex:0];
        if ([validProduct.productIdentifier
             isEqualToString:kTutorialPointProductID]) {
            [productTitleLabel setText:[NSString stringWithFormat:
                                        @"Product Title: %@",validProduct.localizedTitle]];
            [productDescriptionLabel setText:[NSString stringWithFormat:
                                              @"Product Desc: %@",validProduct.localizedDescription]];
            [productPriceLabel setText:[NSString stringWithFormat:
                                        @"Product Price: %@",validProduct.price]];
        }
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
    }    
    [activityIndicatorView stopAnimating];
    purchaseButton.hidden = NO;
}





@end
