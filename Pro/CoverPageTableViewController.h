//
//  CoverPageTableViewController.h
//  Fax Now
//
//  Created by ibuildx-Mac on 7/25/17.
//  Copyright © 2017 ibuildx-Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol selectCoverInfo <NSObject>
@optional;
-(void)receicveCoverInfo:(NSDictionary *)infoDict;
@end


@interface CoverPageTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITextField *txtFrom;
@property (strong, nonatomic) IBOutlet UITextField *txtContacts;
@property (strong, nonatomic) IBOutlet UITextField *txtMessage;
@property (nonatomic, strong) id<selectCoverInfo> delegate; // Delegate Id


- (IBAction)closeAction:(id)sender;
- (IBAction)createAction:(id)sender;

@end
