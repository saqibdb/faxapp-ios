//
//  Country.swift
//  PhoneNumberPicker
//
//  Created by Hugh Bellamy on 06/09/2015.
//  Copyright (c) 2015 Hugh Bellamy. All rights reserved.
//

import Foundation

open class Country: NSObject {
    /// Ex: "RU"
    open var countryCode: String
    /// Ex: "+7"
    open var phoneExtension: String
    @objc open var name: String {
        return NKVLocalizationHelper.countryName(by: countryCode) ?? ""
    }

    public init(countryCode: String, phoneExtension: String) {
        self.countryCode = countryCode
        self.phoneExtension = phoneExtension
    }
    
    /// Returns a Country entity of the current iphone's localization region code
    /// or empty country if it not exist.
    open static var currentCountry: Country {
        guard let currentCountryCode = NKVLocalizationHelper.currentCode else {
            return Country.empty
        }
        return Country.countryByCountryCode(currentCountryCode)
    }
    
    /// Making entities comparable
    override open func isEqual(_ object: Any?) -> Bool {
        if let rhs = object as? Country {
            return countryCode == rhs.countryCode
        }
        return false
    }
    
    // MARK: - Class methods   
    
    /// Returnes an empty country entity for test or other purposes. 
    /// "+" code returns a flag with question mark.
    open static var empty: Country {
        return Country(countryCode: "?", phoneExtension: "")
    }
    
    /// Returns a country by a phone extension.
    ///
    /// - Parameter phoneExtension: For example: "+241"
    open class func countryByPhoneExtension(_ phoneExtension: String) -> Country {
        let phoneExtension = phoneExtension.cutPluses
        for country in NKVSourcesHelper.countries {
            if phoneExtension == country.phoneExtension {
                return country
            }
        }
        return Country.empty
    }
    
    /// Returns a country by a country code.
    ///
    /// - Parameter countryCode: For example: "FR"
    open class func countryByCountryCode(_ countryCode: String) -> Country {
        for country in NKVSourcesHelper.countries {
            if countryCode.lowercased() == country.countryCode.lowercased() {
                return country
            }
        }
        return Country.empty
    }
    
    /// Returns a countries array from the country codes.
    ///
    /// - Parameter countryCodes: For example: ["FR", "EN"]
    open class func countriesByCountryCodes(_ countryCodes: [String]) -> [Country] {
        return countryCodes.map { code in
            Country.countryByCountryCode(code)
        }
    }
}
